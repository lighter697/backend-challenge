## Installation

1. Create a `.env` from the `.env.dist` file. Adapt it according to your symfony application

    ```bash
    cp .env.dist .env
    ```
   
2. Build/run containers with (with and without detached mode)

    ```bash
    $ docker-compose build
    $ docker-compose up -d
    ```
   
3. Update your system host file (add symfony.local)

4. Prepare Symfony app

    1. Update app/config/parameters.yml
    
        ```yml
        parameters:
            database_host: db
            database_port: null
            database_name: symfony
            database_user: user
            database_password: secret
            mailer_transport: smtp
            mailer_host: 127.0.0.1
            mailer_user: null
            mailer_password: null
            secret: ThisTokenIsNotSoSecretChangeIt
            oauth_access_token_lifetime: 3600
            oauth_refresh_token_lifetime: 1209600

        ```

    2. Composer install & create database

        ```bash
        $ docker-compose exec php bash
        $ composer install
        # Symfony2
        $ sf3 doctrine:database:create
        $ sf3 doctrine:schema:update --force
        $ sf3 doctrine:fixtures:load --no-interaction
        ```
       
5. Create oauth client

    ```bash 
    $ php bin/console fos:oauth-server:create-client --grant-type=password --grant-type=refresh_token --grant-type=client_credentials
    ```

6. Take `client_id` and `client_secret` values to create login

    Example login
    
    Create a POST request to http://symfony.local/oauth/v2/token with body
    ```
    {
        "client_id": "<CLIENT_ID>",
        "client_secret": "<CLIENT_SECRET>",
        "grant_type": "password",
        "username": "foo-test@foo.com",
        "password": "secret"
    }
    ```

7. Use `Authorization: Bearer access_token` to authorize your requests

    Go to GET http://symfony.local/product to fetch the list of products
    
    Go to GET http://symfony.local/product/{id} to fetch single product
    
    Go to POST http://symfony.local/product/new to create new product
    
    Go to POST http://symfony.local/product/{id}/edit to update product
    
    Go to DELETE http://symfony.local/product/{id} to delete product
    
    Go to http://symfony.local/category to fetch the list of categories
    
    Go to http://symfony.local/category/{id} to fetch single category
    

