<?php


namespace AppBundle\Transformer;


use AppBundle\Entity\Product;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $availableIncludes = [
        'category'
    ];

    /**
     * @var array
     */
    protected $defaultIncludes = [
        'category'
    ];

    /**
     * @param Product $product
     *
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            'id' => $product->getId(),
            'title' => $product->getTitle(),
            'price' => $product->getPrice(),
            'quantity' => $product->getQuantity(),
            'sku' => $product->getSku(),
            'createdAt' => $product->getCreatedAt()->format('Y-m-d H:i:s'),
            'updatedAt' => $product->getUpdatedAt()->format('Y-m-d H:i:s'),
        ];
    }

    /**
     * @param Product $product
     *
     * @return Item
     */
    public function includeCategory(Product $product)
    {
        if ($product->getCategory()) {
            return $this->item($product->getCategory(), new CategoryTransformer());
        }
    }
}
