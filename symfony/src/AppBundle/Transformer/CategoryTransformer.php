<?php


namespace AppBundle\Transformer;


use AppBundle\Entity\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{

    /**
     * @param Category $category
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'id' => $category->getId(),
            'title' => $category->getTitle(),
        ];
    }
}
