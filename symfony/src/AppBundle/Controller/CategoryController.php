<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Serializer\DataSerializer;
use AppBundle\Transformer\CategoryTransformer;
use AppBundle\Transformer\ProductTransformer;
use FOS\RestBundle\Controller\Annotations as REST;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Category controller.
 *
 * @REST\Route("category")
 */
class CategoryController extends RestController
{
    /**
     * @var Manager $manager
     */
    private $manager;

    public function __construct()
    {
        $this->manager = new Manager();
        $this->manager->setSerializer(new DataSerializer());
    }

    /**
     * Lists all category entities.
     *
     * @REST\Get("/", name="category_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository(Category::class)->findAll();

        $data = $this->manager->createData(new Collection($categories, new CategoryTransformer()));

        return $this->prepareResponse($data->toArray());
    }
}
