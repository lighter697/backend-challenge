<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use AppBundle\Serializer\DataSerializer;
use AppBundle\Transformer\ProductTransformer;
use FOS\RestBundle\Controller\Annotations as REST;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Product controller.
 *
 * @REST\Route("product")
 */
class ProductController extends RestController
{
    /**
     * @var Manager $manager
     */
    private $manager;

    public function __construct()
    {
        $this->manager = new Manager();
        $this->manager->setSerializer(new DataSerializer());
    }

    /**
     * Lists all product entities.
     *
     * @REST\Get("/", name="product_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository(Product::class)->findAll();

        $data = $this->manager->createData(new Collection($products, new ProductTransformer()));

        return $this->prepareResponse($data->toArray());
    }

    /**
     * Creates a new product entity.
     *
     * @REST\Post("/new", name="product_new")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function newAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $data = $this->manager->createData(new Item($product, new ProductTransformer()));

            return $this->prepareResponse($data->toArray());
        }

        return $this->prepareResponse([], $this->getFormErrorsAsArray($form), Response::HTTP_BAD_REQUEST);
    }

    /**
     * Finds and displays a product entity.
     *
     * @REST\Get("/{id}", name="product_show")
     * @param Product $product
     *
     * @return JsonResponse
     */
    public function showAction(Product $product)
    {
        $data = $this->manager->createData(new Item($product, new ProductTransformer()));

        return $this->prepareResponse($data->toArray());
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @REST\Post("/{id}/edit", name="product_edit")
     * @param Request $request
     * @param Product $product
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Product $product)
    {
        $editForm = $this->createForm(ProductType::class, $product);
        $editForm->submit($request->request->all());

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $data = $this->manager->createData(new Item($product, new ProductTransformer()));

            return $this->prepareResponse($data->toArray());
        }

        return $this->prepareResponse([], $this->getFormErrorsAsArray($editForm), Response::HTTP_BAD_REQUEST);
    }

    /**
     * Deletes a product entity.
     *
     * @REST\Delete("/{id}", name="product_delete")
     * @param Request $request
     * @param Product $product
     *
     * @return JsonResponse
     */
    public function deleteAction(Request $request, Product $product)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();

        return $this->prepareResponse([], ['Product has been successfully deleted']);
    }
}
