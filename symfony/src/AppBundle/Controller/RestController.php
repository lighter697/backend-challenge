<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RestController
 */
class RestController extends FOSRestController
{
    /**
     * Get form errors as array
     *
     * @param FormInterface $form
     *
     * @return array
     */
    public function getFormErrorsAsArray(FormInterface $form)
    {
        $errors = [];

        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getFormErrorsAsArray($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }

    protected function prepareResponse($data, $messages = [], $status = Response::HTTP_OK)
    {
        return new JsonResponse([
            'data' => $data,
            'messages' => $messages,
        ], $status);
    }
}
