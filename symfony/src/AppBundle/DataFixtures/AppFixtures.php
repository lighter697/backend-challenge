<?php


namespace AppBundle\DataFixtures;


use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var PasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $seedData = file_get_contents('https://lq3-production.s3-us-west-2.amazonaws.com/coding-challenge/data-fixtures.json');
        $decoded = json_decode($seedData, true);

        foreach ($decoded['products'] as $item) {

            // Find category by it's title
            $category = $manager->getRepository(Category::class)->findOneBy([
                'title' => $item['category'],
            ]);

            // Create new category if doesnt exist
            if (empty($category)) {
                $category = new Category();
                $category->setTitle($item['category']);
            }

            // Create new product
            $product = new Product();
            $product->setTitle($item['name']);
            $product->setSku($item['sku']);
            $product->setPrice(floatval($item['price']));
            $product->setCategory($category);
            $product->setQuantity($item['quantity']);
            $manager->persist($product);
        }

        $manager->flush();


        foreach ($decoded['users'] as $item) {


            $user = new User();
            $user->setFullName($item['name']);
            $user->setUsername($item['email']);
            $user->setUsernameCanonical($item['email']);
            $user->setEmail($item['email']);
            $user->setEnabled(true);
            $user->setEmailCanonical($item['email']);
            $user->setPlainPassword('secret');
            $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPlainPassword()));
            $manager->persist($user);
        }

        $manager->flush();
    }
}
